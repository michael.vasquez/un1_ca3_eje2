# gestionar entradas no numericas con elegancia
# autor = "Michael Vasquez"
# email = "michael.vasquez@unl.edu.ec"

try:
    saldo   =   float(input("Ingrese el numero de horas trabajadas:\n"))
    tarifa  =   float(input("Engrese el monto por hora:\n"))

    total   =   saldo * tarifa
    print("El total a recibir es ", total, " dolares")

except:
    print("La cantidad que Usted ingreso no es un numero")